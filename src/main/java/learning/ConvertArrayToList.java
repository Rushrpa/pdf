package learning;

import java.util.Arrays;
import java.util.List;

public class ConvertArrayToList {
	
	
 public static void main(String[] args) {
	 
	 //String array
	 String[] words = {"ace", "boom", "crew", "dog", "eon"};
	 //Use Arrays utility class
	 List<String> wordList = Arrays.asList(words);
	  //Now you can iterate over the list
	 	 System.out.println(wordList);

 
	 	//String array
	 	Integer[] nums = {1,2,3,4};
	 	//Use Arrays utility class
	 	List<Integer> numsList = Arrays.asList(nums);
	 	System.out.println(numsList); 
	 	
	 	 
 
 }

}
