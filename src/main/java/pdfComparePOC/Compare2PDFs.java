package pdfComparePOC;

import java.io.IOException;

import org.testng.annotations.Test;

import de.redsix.pdfcompare.CompareResult;
import de.redsix.pdfcompare.PdfComparator;

/**
 * Unit test for simple App.
 */
public class Compare2PDFs 
{
	
	

    public boolean ComparePDF(String exp, String act, String resultFil) throws IOException {
    	
//    	final CompareResult result = new PdfComparator(expected, actual).compare();
//    	if (result.isNotEqual()) {
//    	    System.out.println("Differences found!");
//    	}
//    	if (result.isEqual()) {
//    	    System.out.println("No Differences found!");
//    	}
//    	if (result.hasDifferenceInExclusion()) {
//    	    System.out.println("Differences in excluded areas found!");
//    	}
//    	result.getDifferences(); // returns page areas, where differences were found	
    	
//    	 For convenience, writeTo also returns the equals status:
    	boolean isEquals = new PdfComparator(exp, act).compare().writeTo(resultFil);
    	if (!isEquals) {
    	    System.out.println("Differences found!");
    	}
    	
    	return isEquals;
    }
    
    //	PDF Compare result location
//	public String testResults(String resltLoc) {
//		String timeStamp=dateTime.capturingCurrentDateTime();
//		String compareReport="CompareReport_"+timeStamp;
//		String res=resltLoc+compareReport;
//		return res;
//	}
	
}
